<?php
declare(strict_types=1);
require_once 'vendor/autoload.php';

const CREATE_TABLE = true;
const INSERT_USERS = true;
const USERS_NUMBER_TO_INSERT = 70;

/** Script entrypoint */
execute();

/**
 * @return void
 */
function execute()
{
    $start = microtime(true);

    $mysqli = new mysqli('admin-master-1', 'admin', '12345', 'admin');

    if(CREATE_TABLE === true){
        createTable($mysqli);
    }

    if(INSERT_USERS === true){
        for ($i=0; $i < USERS_NUMBER_TO_INSERT; $i++){
            insertUsers($mysqli, sampleUser());
        }
    }

    $mysqli->close();

    $delta = microtime(true) - $start;
    print_r($delta . "\n");
}

/**
 * @param $mysqli
 *
 * @return void
 */
function createTable($mysqli)
{
    $mysqli->query(
        "CREATE TABLE IF NOT EXISTS Users (
            user_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
            first_name varchar(255), 
            last_name varchar(255),
            user_date date
            email varchar(255) NOT NULL, 
        );
    ");
}

/**
 * @param $mysqli
 * @param $user
 *
 * @return void
 */
function insertUsers($mysqli, $user)
{
    $mysqli->query(sprintf("
        INSERT INTO Users (first_name, last_name, user_date, email)
        VALUES ('%s', '%s', '%s', '%s')",
        mysqli_real_escape_string($mysqli, $user['first_name']),
        mysqli_real_escape_string($mysqli, $user['last_name']),
        mysqli_real_escape_string($mysqli, $user['user_date']),
        mysqli_real_escape_string($mysqli, $user['email']),
    ));
}

/**
 * @return array
 */
function sampleUser()
{
    /** @var Faker\Generator $faker */
    $faker = Faker\Factory::create();

    return [
        'first_name' => $faker->firstname(),
        'last_name' => $faker->lastname(),
                'user_date' => $faker->date('Y-m-d'),
        'email' => $faker->email(),
    ];
}