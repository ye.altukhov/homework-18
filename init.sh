#!/bin/bash

echo 'Create user and grant permissions'
docker exec -it b29e7e9cefa5 mysql -uroot -p12345 admin -e "CREATE USER 'slave'@'%' IDENTIFIED WITH mysql_native_password BY '12345'; GRANT REPLICATION SLAVE ON *.* TO 'slave'@'%';"

echo 'Show master status'
docker exec -it b29e7e9cefa5 mysql -uroot -p12345 admin -e "SHOW MASTER STATUS;"